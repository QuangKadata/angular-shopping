import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
})
export class RecipeListComponent implements OnInit {

  @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes: Recipe[];

  constructor(private recipeService: RecipeService) {}

  ngOnInit() {
    console.log('recipe', this.recipes);
    this.recipes = this.recipeService.getRecipes();
    console.log('recipe', this.recipes);
  }

  //recipe được nhận từ thằng con  @Input() recipe: Recipe;
  onRecipeSelected(recipe: Recipe){
   this.recipeWasSelected.emit(recipe);
  }
}
