import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()

export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe(
      'Item 1',
      'This is simply a test',
      'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
      [
        new Ingredient('Meat',1),
        new Ingredient('French Chies',5)
      ]
    ),
    new Recipe(
      'Item 2',
      'This is simply a test',
      'https://cdn-icons-png.flaticon.com/512/1005/1005630.png',
      [
        new Ingredient('Meat Ball',20),
        new Ingredient('Fucking Nigga',5)
      ]
    ),
  ];
  getRecipes() {
    return this.recipes.slice();
  }
  constructor(private slService: ShoppingListService){

  }
  addIngredientToShoppingList(ingredient:Ingredient[]){
    this.slService.addIngredients(ingredient)
  }
}
